import { test } from '@japa/runner'

test.group('Register user', () => {
  test('display register page', async ({ client }) => {
    const response = await client.get('/register')

    response.assertStatus(200)
    response.assertTextIncludes("Inscription")
  })
})
