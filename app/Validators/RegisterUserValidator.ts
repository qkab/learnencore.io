import { schema, CustomMessages, rules } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class RegisterUserValidator {
  constructor(protected ctx: HttpContextContract) { }

  public schema = schema.create({
    username: schema.string({ trim: true }, [rules.unique({ table: 'users', column: 'username', caseInsensitive: true })]),
    email: schema.string({ trim: true }, [rules.email(), rules.unique({ table: 'users', column: 'username', caseInsensitive: true })]),
    password: schema.string({}, [rules.minLength(8)])
  })

  public messages: CustomMessages = {
    required: 'Le champ est requis',
    'username.unique': 'Un compte existe déjà avec ce nom d\'utilisateur',
    'password.minLength': 'Le mot de passe doit contenir au moins 8 caractères',
  }
}
