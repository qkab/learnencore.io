import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User';
import RegisterUserValidator from 'App/Validators/RegisterUserValidator';
import Hash from '@ioc:Adonis/Core/Hash';
import Mail from '@ioc:Adonis/Addons/Mail';
import Env from '@ioc:Adonis/Core/Env';

export default class AuthController {
  public async showRegisterForm({ inertia }) {
    return inertia.render('Register')
  }

  public async register({ request, response, auth }: HttpContextContract) {
    const data = await request.validate(RegisterUserValidator);
    const user = await User.create(data);

    await auth.login(user); 
    await Mail.sendLater((message) => {
      message
      .from('qkabasele@gmail.com')
      .to(user.email)
      .htmlView('emails/verify-email', {
        username: user.username,
        url: `${Env.get('DOMAIN')}/verify-email/${user.id}`
      })
    });

    return response.redirect('/');
  }

  public async showLoginForm({ inertia }: HttpContextContract) {
    return inertia.render('Login')
  }

  public async login({ auth, request, response }: HttpContextContract) {
    const { email, password } = request.only(['email', 'password']);

    try {
      await auth.attempt(email, password);
    } catch (error) {
      await Hash.verify(await Hash.make('test'), 'secret')
      throw error;
    }

    return response.redirect('/');
  }

  public async logout({ auth, response }: HttpContextContract) {
    await auth.logout();

    return response.redirect('/');
  }
}
