import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User';

export default class VerifyEmailsController {
  public async verify({ params, response, session }: HttpContextContract) {
    const user = await User.findOrFail(params.id);

    if (user.isEmailVerified) {
      session.flash('success', 'Your email has already been verified');
      return response.redirect('/');
    }

    user.isEmailVerified = true;
    await user.save();
    session.flash('success', 'Your email has been verified');

    return response.redirect('/');
  }
}
