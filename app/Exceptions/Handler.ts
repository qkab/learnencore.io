/*
|--------------------------------------------------------------------------
| Http Exception Handler
|--------------------------------------------------------------------------
|
| AdonisJs will forward all exceptions occurred during an HTTP request to
| the following class. You can learn more about exception handling by
| reading docs.
|
| The exception handler extends a base `HttpExceptionHandler` which is not
| mandatory, however it can do lot of heavy lifting to handle the errors
| properly.
|
*/

import Logger from '@ioc:Adonis/Core/Logger'
import HttpExceptionHandler from '@ioc:Adonis/Core/HttpExceptionHandler'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { CustomErrorTypes } from '../../types/errors'
export default class ExceptionHandler extends HttpExceptionHandler {
  protected statusPages = {
    '403': 'errors/unauthorized',
    '404': 'errors/not-found',
    '500..599': 'errors/server-error',
  }

  constructor() {
    super(Logger)
  }

  public async handle(error: any, ctx: HttpContextContract): Promise<any> {
    const { session, response } = ctx;

    if ([CustomErrorTypes.INVALID_AUTH_PASSWORD, CustomErrorTypes.INVALID_AUTH_UID].includes(error.code)) {
      Logger.error(error);
      session.flash('errors', { login: 'Aucun compte n\'a été trouvé avec ces identifiants.' });
      return response.redirect('/login');
    }

    if (error.code === CustomErrorTypes.UNIQUE_VIOLATION) {
      Logger.error(error);
      session.flash('errors', { register: 'Une erreur est survenue lors de l\'inscription de l\'utilisateur.' });
      return response.redirect('/register');
    }

    if (error.code === CustomErrorTypes.INVALID_CSRF_TOKEN) {
      Logger.error(error);
      session.flash('errors', { login: 'Une erreur est survenue lors de la connexion de l\'utilisateur.' });
      return response.redirect('/login');
    }

    return super.handle(error, ctx)
  }
}
