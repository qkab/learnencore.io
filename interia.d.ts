import type { Page, PageProps, Errors, ErrorBag } from '@inertiajs/inertia';

declare module '@inertiajs/core' {
  interface PageProps extends Page<PageProps> {
    user: {
      id: string
      email: string
      username: string
    },
    csrfToken: string,
    success: string,
  }
}