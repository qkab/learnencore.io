import { Link, router, usePage } from '@inertiajs/react'
import React from 'react'

const index = () => {
  const { user } = usePage().props

  const handleSubmit = (e) => {
    e.preventDefault();

    return router.post('/logout?_method=DELETE')
  }

  return (
    <header className='flex flex-col gap-3 p-3 border-b-2'>
      <h1>Learnencore.io</h1>
      <nav className='flex flex-col'>
        <Link href="/">Accueil</Link>
        <Link href="/test">Test</Link>
        {!user ? (
          <>
            <Link href="/login">Connexion</Link>
            <Link href="/register">Inscription</Link>
          </>
        ) : (
          <form onSubmit={handleSubmit} method='POST'>
            <button type='submit'>Déconnexion ({user.username})</button>
          </form>
        )}
      </nav>
    </header>
  )
}

export default index