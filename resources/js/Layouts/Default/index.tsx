import React, { ReactNode } from 'react'
import Header from '../../Components/Header'
import { usePage } from '@inertiajs/react'

const DefautlLayout = ({ children }: { children: ReactNode }) => {
  const { success } = usePage().props

  return (
    <main className='flex flex-col gap-3 w-full'>
      <Header />
      <div className="p-3 relative">
        {success && (
          <div className="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative" role="alert">
            <span className="block sm:inline">{ success }</span>
          </div>
        )}
        {children}
      </div>
    </main>
  )
}

export default DefautlLayout