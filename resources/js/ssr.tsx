import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { createInertiaApp } from '@inertiajs/react';
import DefautlLayout from './Layouts/Default';

export default function render(page) {
  return createInertiaApp({
    page,
    render: ReactDOMServer.renderToString,
    resolve: (name) => {
      const page = require(`./Pages/${name}`).default
      page.layout = page.layout || ((page) => <DefautlLayout children={page} />)
      return page
    },
    setup: ({ App, props }) => <App {...props} />,
  });
}