import { PageProps } from '@inertiajs/core';
import { usePage } from '@inertiajs/react'
import React from 'react'

const Home = () => {
  const { user } = usePage<PageProps>().props;
  
  return (
    <div className='flex flex-col p-3'>
      <h2>Home Page</h2>
      {user && <span className='capitalize'>Hello {user.username}</span>}
    </div>
  )
}

export default Home