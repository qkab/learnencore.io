import { router, usePage } from '@inertiajs/react'
import React, { useState } from 'react'


const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { errors } = usePage().props

  const handleSubmit = (e) => {
    e.preventDefault();

    return router.post('/login', {
      email,
      password,
    })
  };

  return (
    <div className='flex flex-col gap-3'>
      {errors?.login && (
        <div className='border border-red-400 text-red-700 px-4 py-3 rounded relative' role='alert'>
          <strong className='font-bold'>{errors.login}</strong>
        </div>
      )}

      <h2>Page de connexion</h2>
      <form onSubmit={handleSubmit} method='POST'>
        <div className="flex flex-col gap-3">
          <label htmlFor="email">Email</label>
          <input className='border' type="email" name="email" id="email" onChange={(e) => setEmail(e.target.value)} />
        </div>
        <div className="flex flex-col gap-3">
          <label htmlFor="password">Password</label>
          <input className='border' type="password" name="password" id="password" onChange={(e) => setPassword(e.target.value)} />
        </div>
        <button className='border p-3' type="submit">Login</button>
      </form>
    </div>
  )
}

export default Login