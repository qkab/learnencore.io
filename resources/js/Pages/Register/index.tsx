import { router, usePage } from '@inertiajs/react'
import React, { useState } from 'react'


const Register = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { errors } = usePage().props

  const handleSubmit = (e) => {
    e.preventDefault();

    return router.post('/register', {
      username,
      email,
      password,
    })
  };

  return (
    <div className='flex flex-col gap-3'>
      {errors?.register && (
        <div className='relative px-4 py-3 text-red-700 border border-red-400 rounded' role='alert'>
          <strong className='font-bold'>{errors.register}</strong>
        </div>
      )}

      <h2>Inscription</h2>
      <form onSubmit={handleSubmit} method='POST'>
        <div className="flex flex-col gap-3">
          <label htmlFor="username">Nom d'utilisateur</label>
          <input className={`border ${errors?.username ? 'border-red-500' : ''}`} type="username" name="username" id="username" onChange={(e) => setUsername(e.target.value)} />
          {errors?.username && (
            <span className='text-red-500'>{errors.username[0]}</span>
          )}
        </div>
        <div className="flex flex-col gap-3">
          <label htmlFor="email">Email</label>
          <input className={`border ${errors?.email ? 'border-red-500' : ''}`} type="email" name="email" id="email" onChange={(e) => setEmail(e.target.value)} />
          {errors?.email && (
            <span className='text-red-500'>{errors.email[0]}</span>
          )}
        </div>
        <div className="flex flex-col gap-3">
          <label htmlFor="password">Mot de passe</label>
          <input className={`border ${errors?.password ? 'border-red-500' : ''}`}  type="password" name="password" id="password" onChange={(e) => setPassword(e.target.value)} />
          {errors?.password ? (
            <span className='text-red-500'>{errors.password[0]}</span>
          ) : (
            <span className='text-xs text-gray-500'>*Le mot de passe doit inclure 8 caractère minimum.</span>
          )}
        </div>
        <button className='p-3 border' type="submit">Inscription</button>
      </form>
    </div>
  )
}

export default Register