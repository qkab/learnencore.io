/*
|--------------------------------------------------------------------------
| Inertia Preloaded File
|--------------------------------------------------------------------------
|
| Any code written inside this file will be executed during the application
| boot.
|
*/

import Inertia from '@ioc:EidelLev/Inertia';

Inertia.share({
  errors: (ctx) => {
    return ctx.session.flashMessages.get('errors');
  },
  csrfToken: ({ request, response }) => {
    const responseCookies = response.getHeader('set-cookie');

    if (typeof responseCookies === 'string') {
      const cookies = request.header('cookie')?.split('; ');
      const csrfToken = cookies?.find(cookie => cookie.startsWith('XSRF-TOKEN'))?.split('=')[1];
      return decodeURIComponent(csrfToken!);
    }
  },
  user: (ctx) => {
    const user = ctx.auth.user;
    return user ? {
      id: user.id,
      username: user.username,
      email: user.email,
    } : null;
  },
  success: (ctx) => {
    return ctx.session.flashMessages.get('success');
  }
}).version(() => Inertia.manifestFile('inertia/ssr/manifest.json'));
