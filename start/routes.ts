/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async ({ inertia }) => {
  return inertia.render('Home', { title: 'LearnEncore.io' })
})

Route.get('/login', 'AuthController.showLoginForm').as('showLoginForm')
Route.post('/login', 'AuthController.login').as('login')

Route.get('/register', 'AuthController.showRegisterForm').as('showRegisterForm')
Route.post('/register', 'AuthController.register').as('register')

Route.delete('/logout', 'AuthController.logout').as('logout')

Route.get('/verify-email/:id', 'VerifyEmailsController.verify').as('verifyEmails')